import { Injectable } from '@angular/core';
import { HttpModule, Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Configuration } from '../../app.constant';

@Injectable()
export class PromotedPostService {
    public Url: string;

    constructor(public http: Http, public _config: Configuration) { }

    getPromotedPost(offset, limit): Observable<any> {
        let url = this._config.Server + "promotedPosts?offset=" + offset + "&limit=" + limit;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getUserDetail(user): Observable<any> {
        let url = this._config.Server + "userDetail?username=" + user;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getPostDetails(postId, postby): Observable<any> {
        let url = this._config.Server + "posts/" + postId + "?username=" + postby;
        // console.log(url);
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
}